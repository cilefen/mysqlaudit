#!/bin/bash
tmp=/tmp/showgrant$$
read -rp "Enter host: " host
read -rp "Enter mysql options: " options
read -rp "Enter user: " username
read -srp "Enter password: " password
mysql -h "$host" --user="$username" --password="$password" "$options" --batch --skip-column-names -e "SELECT concat('\'',User,'\'@\'',Host,'\'') AS UserHost FROM user ORDER BY User, HOST" mysql > $tmp
cat $tmp | while read -r user
    do
        echo "# $user"
        mysql -h "$host" --user="$username" --password="$password" "$options" --batch --skip-column-names -e"SHOW GRANTS FOR $user"
    done
rm $tmp
